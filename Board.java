import java.util.Random;

public class Board {
	private Tile[][] grid;

	public Board() {
		Random rng = new Random();
		
		this.grid = new Tile[5][5];
		for (int row = 0; row < grid.length; row++) {
			int randIndex = rng.nextInt(grid[row].length);
			for (int col = 0; col < grid.length; col++) {
				if(col == randIndex) {
					grid[row][col] = Tile.HIDDEN_WALL;
				} else { grid[row][col] = Tile.BLANK; }
			}
		}
	}
	public String toString() {
		String board = "";
		for (int row = 0; row < grid.length; row++) {
			for (int col = 0; col < grid.length; col++) {
				board += grid[row][col].getName() + " ";
			}
			board += "\n";
		}
		return board;
	}

    public int placeToken(int row, int col) {
		if (row < 0 || row >= grid.length || col < 0 || col >= grid.length) {
			return -2;
		}
		if (grid[row][col] == Tile.WALL || grid[row][col] == Tile.CASTLE) {
			return -1;
		}
		if (grid[row][col]  == Tile.HIDDEN_WALL) {
			grid[row][col] = Tile.WALL;
			return 1;
		}
		if (grid[row][col]  == Tile.BLANK) {
			grid[row][col] = Tile.CASTLE;
			return 0;
		}
		//fake return statement to avoid error
		return -3;
	}	
}
