import java.util.Scanner;

public class BoardGameApp {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Welcome to the wall castle game?");
		
		int numCastles = 7;
		int turns = 0;
		
		Board board = new Board();
		System.out.println(board);
		
		while(numCastles > 0 && turns < 8) {
			
			System.out.println("Where do you want to place your castle? (enter row and then col)");
			int row = scanner.nextInt() - 1;
            int col = scanner.nextInt() - 1;
			
			int placeTokenVal = board.placeToken(row, col);
			
			System.out.println(board);
			
			if (placeTokenVal < 0) {
				System.out.println("Invalid position! Please try again");
                System.out.println("Where do you want to place your castle? (enter row and then col)");
				row = scanner.nextInt();
				col = scanner.nextInt();
			}
			
			if (placeTokenVal == 1) {
				System.out.println("There was a wall at that position");
				turns--;
			}
			
			if (placeTokenVal == 0) {
				System.out.println("A castle tile was successfully places");
				turns--;
				numCastles--;
			}
		}
		System.out.println(board);
		
		if (numCastles == 0) {
			System.out.println("You won!");
		} else { System.out.println("You lost!"); }
	}	
}